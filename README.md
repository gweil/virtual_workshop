# Virtual_workshop

WebGL application for virtual assembly of mechanical systems made with [Three.js](https://threejs.org/) for Prof. Alex Ballu of [I2M](https://www.i2m.u-bordeaux.fr/) (Institut de Mécanique et d'Ingénierie). The application is meant to help the students to exercise virtually concepts they have seen in class.

The application has many features :

  - Simulate a workshop for the students.
  - Load / Unload parts in the workshop.
  - Assemble / Disassemble the parts loaded.
  - Change parts to transparent to observe how mechanical systems work.
  - Isolated view of the parts.
  - Switch between 3D and sectional view.
  - Can have as many languages as desired.
  - Works with configuration files.
  - Fast and lightweight !

## Quickstart

This application is meant to work on the University of Bordeaux [Moodle](https://moodle1.u-bordeaux.fr/). If you want to try the application on a computer use the following command for development.

### Project setup
```shell
npm install
```

### Compiles and hot-reloads for development
```shell
npm run start
```

### Compiles and minifies for production
```shell
npm run build
```

## Help

The [virtual_workshop documentation](docs/README.md) is the best place if you're looking for answers, if you can't find an answer to your question in the documentation please [open an issue](https://gitlab.com/gweil/virtual_workshop/issues).

## Credits

I am not a designer so I've used icons I found on the Internet. Here's the list of the people/sites :

- Emil Persson, aka [Humus](www.humus.name), for the skybox.
- Nikita Golubev from [Flaticon](www.flaticon.com)
- Smashicons from [Flaticon](www.flaticon.com)
- [shashank singh](https://thenounproject.com/rshashank19/) from the Noun Project
- [Feathericons](https://feathericons.com/)
- [Gitlab Logo](https://about.gitlab.com/press/press-kit/)

## License

GNU GPLv3
