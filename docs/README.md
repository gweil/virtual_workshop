# Documentation

virtual_workshop is a WebGL application.

We use [npm](https://www.npmjs.com/) as package manager, [webpack](https://webpack.js.org/) as module bundler, [babel](https://babeljs.io) as compiler, [UglifyJS](https://www.npmjs.com/package/uglifyjs-webpack-plugin) as JavaScript minifier, [papaparse](https://www.npmjs.com/package/papaparse) as CSV parser and [three.js](https://threejs.org/) as WebGL library.

You will not find any information on how all of that works together here, but you can still look at the [npm config file](package.json), the [babel config file](.babelrc) or the [webpack config file](webpack.config.js).

## Usage

### Application

![User interface of the virtual_workshop](images/UI.png)

The UI is composed of buttons in the upper left, lower left and lower right corners.<br>

  - The buttons in the upper left are used to show the Parts and Tools menus (container on the left side of the screen). For each element of the menu the user can load/unload it, toggle the isolated view and change the part to transparent.

  - The lower left buttons are used to select the language of the application. The available languages are set by the user in the configuration files (see ["_configuration files_"](configuration.md)) and the interface updates the names and descriptions instantly.

  - The lower right buttons are used to switch between 3D and sectional view of the loaded parts in the scene, and to switch between fullscreen and window mode.

When the user clicks on a part in the scene the action menu appears. In that menu the user can assemble/disassemble a part, set/unset a part to transparent, toggle the isolated view and unload a part if the selected part is disassembled.

![The isolated view allows the user to load a part and to observe it outside of the scene.](images/isolated.png)

### Color palette

The colors used by the application are set in [_general.css_](/css/general.css).

**Note:** The background color of the isolated view is hard coded in _js/isolatedView.js_ !

### Custom models

You can change the models in _static/parts_ for your own models and configure the program with your own [configuration files](configuration.md). One thing to keep in mind is that 1 unit represents 1mm in real life. That way you can create models that will be scaled correctly in the scene.
