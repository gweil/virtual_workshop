# Configuration files

As mentionned the application works with configuration files that allow teachers to create specific scenarios for their students. The program uses 4 .csv files :

**Note:** Two versions of this project exist, one with Unity, and this one with Three.js. The idea was to find out which one would be the lightest, fastest and easiest to maintain. That's why the \_common files have more data than needed. The \_common files are not used in this application but can be used for further features.

**Note:** In this version the UI is only graphical. That way the application is more intuitive and we don't have to load to many information to describe a single action like "Assemble".

  - #### _**general.csv**_
  Gives the default language of the application and the languages available. The language need to follow the syntax aa-AA.<br>
  **Used in loadLanguages() of _js/languages.js_.**

  - #### _**state.csv**_
  Gives the position and the rotation made by the part when it's assembled or disassembled. One part has two lines, the first one is the assembled state and the following line is the disassembled state. _Axis_ is a normalized vector that represents the rotation direction, _Angle_ is in rad (you can write "PI" instead of an approximation) and _Point_ is a point in space. _Position Vector_ is the position when the part in the corresponding state.<br>
  **Used in extractStateData() of _js/partsManager.js_.**

  - #### _**action.csv**_
  Gives the contraints to assemble/disassemble a part. It works in a similar way than _state.csv_ : the first line is for assembly and the following for disassembly.<br>
  **Used in extractActionData() of _js/partsManager.js_.**

  - #### _**obj.csv**_
  Gives all the informations relative to the parts themselves :
    
    - ID.
    - Names and descriptions in the different languages.
    - Name of the file for the picture in the menu.
    - Names of the .obj files for 3D and sectional views.
    - Menu to be displayed in ("Parts" or "Tools").
  **Used in extractObjData() of _js/partsManager.js_.**
