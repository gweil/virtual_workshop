const sceneManager = require('./sceneManager');
const eventHandler = require('./eventHandler');

// ------------ Global variables ------------

global.WIDTH = window.innerWidth;
global.HEIGHT = window.innerHeight;
global.APP_LANGUAGE = "";


let init = () => {
  eventHandler.initEvents().then( () => {
    document.getElementById('loadingScreen').style.display = 'none';
    render();
  }).catch( error => { alert(error) } );
}


let updateLanguageUI = () => {
  let selector = document.getElementById('languageSelector');
  for (let language of selector.children) {
    if (language.id === APP_LANGUAGE) {
      for (let element of document.getElementsByClassName(language.id)) {
        element.style.display = 'contents';
      }
    } else {
      for (let element of document.getElementsByClassName(language.id)) {
        element.style.display = 'none';
      }
    }
  }
}


let render = () => {
  requestAnimationFrame(render);
  updateLanguageUI();
  sceneManager.updateScene(WIDTH, HEIGHT);
  sceneManager.renderScene();
}


init();
