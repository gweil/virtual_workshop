let sceneManager = require('./sceneManager');

let mouseClicked = false, mouseMoved = false;
let cursorX, cursorY, partHoveredInList;
let loadingDone = false;


export let initEvents = () => {
  return new Promise((resolve, reject) => {
    window.addEventListener('resize', onWindowResize, false);
    window.addEventListener('mousemove', evt => { onMouseMove(evt) });

    document.getElementById('btnParts').addEventListener("click", () => { showList('parts') });
    document.getElementById('btnTools').addEventListener("click", () => { showList('tools') });
    document.getElementById('languageSelector').addEventListener("click", evt => {
      selectLanguage(evt, document.getElementById('languageSelector')) });
    document.getElementById('actionsMenu').addEventListener("click", evt => { actionOnClick(evt) });
    document.getElementById('output').addEventListener("mousedown", onMouseDown);
    document.getElementById('output').addEventListener("click", clickOnOutput);
    document.getElementById('screenMode').addEventListener("click", toggleFullscreen);
    document.getElementById('partsView').addEventListener("click", evt => { changePartsView(evt) });
    document.getElementById('exitMenu').addEventListener("click", hideMenu);
    document.getElementById('exitActions').addEventListener("click", hideActions);
    document.getElementById('exitIsolated').addEventListener("click", closeIsolated);
    document.getElementById('isolatedDiv').addEventListener("click", closeIsolated);

    // initScene returns the id of the default language of the application
    sceneManager.initScene(output).then( dft => {
      document.getElementById(dft).style.backgroundColor = 'var(--second-color)';
      APP_LANGUAGE = dft;

      // Descriptions when hover on part/tools names
      for (let list of document.getElementsByClassName('list'))
        for (let itemOfList of list.getElementsByTagName('P'))
          itemOfList.addEventListener("mouseover", () => {
            for (let description of itemOfList.getElementsByClassName('desc'))
              partHoveredInList = description;
          });
      for (let btn of document.getElementsByClassName('transparent'))
        btn.addEventListener("click", evt => { switchTransparentPart(evt.target.id) });
      for (let btn of document.getElementsByClassName('isolated'))
        btn.addEventListener("click", evt => { openIsolated(evt.target.id) });
      for (let btn of document.getElementsByClassName('loadBtn'))
        btn.addEventListener("click", evt => { loadPartInScene(evt.target.id) });
      for (let btn of document.getElementsByClassName('unloadBtn'))
        btn.addEventListener("click", evt => { unloadPartInScene(evt.target.id) });
      resolve();
    }).catch(error => { reject(error) });
  });
}


let onWindowResize = () => {
  WIDTH  = window.innerWidth;
  HEIGHT = window.innerHeight;
}


let onMouseMove = evt => {
  if (mouseClicked) { mouseMoved = true; }
  cursorX = evt.clientX;
  cursorY = evt.clientY;
  if (partHoveredInList !== undefined) {
    partHoveredInList.style.top = (cursorY + 10) + 'px';
    partHoveredInList.style.left = (cursorX + 10) + 'px';
  }
}


let showList = list => {
  let div = document.getElementById('menu');
  div.style.display = "flex";
  let tools = document.getElementById('listTools');
  let parts = document.getElementById('listParts');
  if (list === "parts") {
    tools.style.display = "none";
    parts.style.display = "flex";
  } else {
    tools.style.display = "flex";
    parts.style.display = "none";
  }
}


let selectLanguage = (evt, selector) => {
  if (evt.target !== selector) {
    sceneManager.selectLanguage(evt.target.id);
    for (let child of selector.children) {
      child.style.backgroundColor = 'var(--main-color)';
    }
    evt.target.style.backgroundColor = 'var(--second-color)';
    APP_LANGUAGE = evt.target.id;
  }
}


let actionOnClick = evt => {
  if (evt.target.parentElement.className === "actions") {
    if (sceneManager.actionToDo(evt.target.id)) {
      switch (evt.target.id) {
        case 'assemble':
          document.getElementById('assemble').style.display = 'none';
          document.getElementById('disassemble').style.display = 'inline-block';
          document.getElementById('unload').style.display = 'none';
          break;
        case 'disassemble':
          document.getElementById('disassemble').style.display = 'none';
          document.getElementById('assemble').style.display = 'inline-block';
          document.getElementById('unload').style.display = 'inline-block';
          break;
        default:
          break;
      }
    }
  }
  hideActions();
}


let onMouseDown = () => {
  mouseClicked = true;
}


let clickOnOutput = () => {
  if (mouseClicked && !mouseMoved) {
    // Raycasting to check if user click on part in the scene.
    let x = (cursorX/WIDTH) * 2 - 1;
    let y = - (cursorY/HEIGHT) * 2 + 1;

    let part = sceneManager.detectPart(x, y);

    if (part.isClicked) {
      document.getElementById('actionsMenu').style.top = cursorY + 'px';
      document.getElementById('actionsMenu').style.left = cursorX + 'px';
      document.getElementById('actionsMenu').style.display = 'flex';

      if (part.isAssembled) {
        document.getElementById('disassemble').style.display = 'inline-block';
        document.getElementById('assemble').style.display = 'none';
        document.getElementById('unload').style.display = 'none';
      } else {
        document.getElementById('disassemble').style.display = 'none';
        document.getElementById('assemble').style.display = 'inline-block';
        document.getElementById('unload').style.display = 'inline-block';
      }

      if (part.isTransparent) {
        document.getElementById('isOff').style.display = 'none';
        document.getElementById('isOn').style.display = 'inline-block';
      } else {
        document.getElementById('isOn').style.display = 'none';
        document.getElementById('isOff').style.display = 'inline-block';
      }
    } else {
      document.getElementById('actionsMenu').style.display = 'none';
    }
  }
  mouseClicked = false;
  mouseMoved = false;
}


let changePartsView = evt => {
  evt.target.id === 'defaultView' ?
    ( document.getElementById('defaultView').style.display = 'none',
      document.getElementById('sectionalView').style.display = 'block',
      sceneManager.switchToSectionalView() ) :
    ( document.getElementById('defaultView').style.display = 'block',
      document.getElementById('sectionalView').style.display = 'none',
      sceneManager.switchToDefaultView() );
}


let toggleFullscreen = () => {
  let elem = document.body;
  if(!document.fullscreenElement) {
    elem.requestFullscreen().catch(err => {
      alert(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`);
    });
    document.getElementById('full').style.display = 'none';
    document.getElementById('window').style.display = 'block';
  } else {
    document.exitFullscreen();
    document.getElementById('full').style.display = 'block';
    document.getElementById('window').style.display = 'none';
  }
}


let hideMenu = () => {
  let div = document.getElementById('menu');
  div.style.display = "none";
}


let hideActions = () => {
  document.getElementById('actionsMenu').style.display = 'none';
}


let switchTransparentPart = btnId => {
  let separator = btnId.lastIndexOf(' ');
  let id = -1;
  let sameBtn, otherBtnMenu, otherBtnAction;

  if (separator !== -1)
    id = btnId.substr(separator + 1, btnId.length);
  let idSelectedPartInScene = sceneManager.switchTransparentPartInScene(id);

  if (id === -1) {  // Transparent button in actions menu
    sameBtn = btnId + ' ' + idSelectedPartInScene;

    btnId === 'isOff' ?
      (otherBtnAction = 'isOn', otherBtnMenu = 'isOn ' + idSelectedPartInScene):
      (otherBtnAction = 'isOff', otherBtnMenu = 'isOff ' + idSelectedPartInScene);

    document.getElementById(sameBtn).style.display = 'none';
    document.getElementById(otherBtnAction).style.display = 'flex';
  } else {  // Transparent button in parts menu
    if (idSelectedPartInScene === id) {
      btnId === 'isOff ' + id ? sameBtn = 'isOff' : sameBtn = 'isOn';
      btnId === 'isOff ' + id ? otherBtnAction = 'isOn' : otherBtnAction = 'isOff';

      document.getElementById(sameBtn).style.display = 'none';
      document.getElementById(otherBtnAction).style.display = 'flex';
    }

    btnId === 'isOff ' + id ?
      otherBtnMenu = 'isOn ' + id :
      otherBtnMenu = 'isOff ' + id;
  }
  document.getElementById(btnId).style.display = 'none';
  document.getElementById(otherBtnMenu).style.display = 'flex';
}


let openIsolated = btnId => {
  setTimeout( () => {
    if (!loadingDone)
      document.getElementById('loadingScreen').style.display = 'block';
  }, 25);

  let id = btnId.lastIndexOf(' ');
  if (id !== -1)
    id = btnId.substr(id + 1, btnId.length);
  document.getElementById('isolatedDiv').style.display = 'flex';
  document.getElementById('isolatedOutput').style.display = 'flex';
  document.getElementById('exitIsolated').style.display = 'flex';
  sceneManager.openIsolatedView(id, WIDTH*0.6, HEIGHT*0.7).then( () => {
    loadingDone = true;
    document.getElementById('loadingScreen').style.display = 'none';
  }).catch( err => { alert(err); });
}


let closeIsolated = () => {
  document.getElementById('isolatedDiv').style.display = 'none';
  document.getElementById('isolatedOutput').style.display = 'none';
  document.getElementById('exitIsolated').style.display = 'none';
  sceneManager.closeIsolatedView();
}


let loadPartInScene = btnId => {
  document.getElementById('loadingScreen').style.display = 'block';
  let id = btnId.substr(btnId.lastIndexOf(' ') + 1, btnId.length);

  sceneManager.loadPartInScene(id).then( value => {
    document.getElementById('loadingScreen').style.display = 'none';
    if (value) {
      document.getElementById("transparent " + id).style.display = "flex";
      document.getElementById("isOff " + id).style.display = "flex";
    }
  }).catch( err => {
    document.getElementById('loadingScreen').style.display = 'none';
    alert(err);
  });
}


let unloadPartInScene = btnId => {
  let id = -1;
  if (btnId !== "unload")
    id = btnId.substr(btnId.lastIndexOf(' ') + 1, btnId.length);
  let unloading = sceneManager.unloadPartInScene(id);
  if (unloading[0]) {
    document.getElementById("transparent " + unloading[1]).style.display = "none";
    document.getElementById("isOff " + unloading[1]).style.display = "none";
    document.getElementById("isOn " + unloading[1]).style.display = "none";
    if (id === -1)
      document.getElementById("actionsMenu").style.display = "none";
  }
}
