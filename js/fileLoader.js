const Papa = require('papaparse');

export const getConfigFile = url => {
  return new Promise( (resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.responseType = "text";
    xhr.onload = function() {
      if (this.status < 300 && this.status >= 200) {
        resolve(Papa.parse(xhr.response, { delimiter: ";" }));
      } else {
        reject(null);
      }
    }
    xhr.onerror = function() {
      reject(null);
    }
    xhr.send();
  })
}

export default getConfigFile;
