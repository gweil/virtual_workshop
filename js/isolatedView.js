let isolatedScene, isolatedRenderer, isolatedCamera, isolatedControls;
let isolatedRatioScene, isolatedWidth, isolatedHeight;
let isolatedInitDone = false;


export let openIsolated = (obj, w, h) => {
  if (!isolatedInitDone) initIsolated(w, h);
  isolatedScene.add(obj);

  let bbox = new THREE.Box3().setFromObject(obj)
  let maxBox = bbox.max.clone();
  let minBox = bbox.min.clone();

  let centerObj = maxBox.clone().add(minBox).divideScalar(2);
  isolatedCamera.lookAt(centerObj);
  isolatedControls.target.set(centerObj.x, centerObj.y, centerObj.z);

  let fov = isolatedCamera.fov * (Math.PI / 180);
  let objectSize = Math.max(maxBox.x - minBox.x, maxBox.y - minBox.y);
  objectSize = Math.max(objectSize, maxBox.z - minBox.z);
  let dist = Math.abs(objectSize / Math.sin(fov / 2));

  isolatedCamera.position.set(centerObj.x, centerObj.y * 1.1, dist);

  isolatedCamera.updateProjectionMatrix();
  isolatedControls.update();

  renderIsolated();
}


export let closeIsolated = () => {
  isolatedScene.remove(isolatedScene.getObjectByName("obj3D"));
  isolatedRenderer.clear();
  isolatedRenderer.dispose();
}


export let update = (w, h) => {
  if (isolatedInitDone) {
    isolatedWidth = w*0.6;
    isolatedHeight = h*0.7;
    isolatedRatioScene = isolatedWidth / isolatedHeight;
    isolatedRenderer.setSize(isolatedWidth, isolatedHeight);
    isolatedCamera.aspect = isolatedRatioScene;
    isolatedCamera.updateProjectionMatrix();

    isolatedControls.update();
  }
}


export let renderIsolated = () => {
  isolatedRenderer.render(isolatedScene, isolatedCamera);
}


let initIsolated = (w, h) => {
  isolatedWidth = w;
  isolatedHeight = h;
  isolatedRatioScene = w / h;

  isolatedScene = new THREE.Scene();
  isolatedScene.background = new THREE.Color(0x33313B);

  isolatedRenderer = new THREE.WebGLRenderer({ canvas: isolatedOutput, antialias: true });
  isolatedRenderer.setSize(isolatedWidth, isolatedHeight);

  addLights();

  isolatedCamera = new THREE.PerspectiveCamera(45, isolatedRatioScene, 0.001, 1000);
  isolatedScene.add(isolatedCamera);

  isolatedControls = new THREE.OrbitControls(isolatedCamera, isolatedRenderer.domElement);

  isolatedInitDone = true;
}


let addLights = () => {
  let hemiLight = new THREE.HemisphereLight(0xFDB8C8);
  hemiLight.groundColor.set(0xeeeeee);
  hemiLight.position.set(0, 10, 0);
  isolatedScene.add(hemiLight);

  let pointLight = new THREE.PointLight(0xffffff);
  pointLight.position.set(0, 0.5, 0);
  isolatedScene.add(pointLight);
}
