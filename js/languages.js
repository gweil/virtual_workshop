const fl = require('./fileLoader');

class Languages {
  constructor(byDefault, available) {
    this.byDefault = byDefault;
    this.areAvailable = available;
  }
}

export const loadLanguages = () => {
  return new Promise( resolve => {
    fl.getConfigFile("config/general.csv").then( generalData => {
      let data = generalData.data;
      let len = data[0].length - 1;
      let byDefault = data[2][len - 1].substr(0, 2) + data[2][len - 1].substr(3, 4);
      let areAvailable = [];
      for(let i = 2; i < data.length; i++) {
        if (data[i].length <= 1) break;
        let languageAvailable = data[i][len];
        let languageId = data[i][len].substr(0, 2) + data[i][len].substr(3, 4);
        let selected;
        languageId === byDefault ? selected = true : selected = false;
        areAvailable.push({
          id: languageId,
          name: languageAvailable,
          isSelected: selected
        });
        let newItem = "<button class=\"secondaryBtn\" id=\""+ languageId +
                      "\">" + languageAvailable + "</button>\n";

        document.getElementById('languageSelector').innerHTML += newItem;
      }
      resolve(new Languages(byDefault, areAvailable));
    }).catch( error => { alert(error) } );
  });
}
