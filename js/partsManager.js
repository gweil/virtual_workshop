require('three/examples/js/loaders/OBJLoader');
require('three/examples/js/loaders/MTLLoader');

const fl = require('./fileLoader');


class Part {
  constructor(id, name, description, picture, views, currentState, states, actions, isAssembled) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.picture = picture;
    this.views = views;
    this.currentState = currentState;
    this.states = states;
    this.actions = actions;
    this.isAssembled = isAssembled;
  }
}


export const loadPartsInformations = languages => {
  return new Promise( (resolve, reject) => {
    let setOfParts = {};
    let states, actions;

    fl.getConfigFile("config/state.csv").then( data => {
      if (data !== null) {
        states = extractStateData(data.data);
      } else {
        reject('Error while parsing config file (see config/state.csv).');
      }

      fl.getConfigFile("config/action.csv").then( data => {
        if (data !== null) {
          actions = extractActionData(data.data);
        } else {
          reject('Error while parsing config file (see config/action.csv).');
        }

        fl.getConfigFile("config/obj.csv").then( data => {
          if (data !== null) {
            let objData = data.data;
            for (let line = 2; line < objData.length; line++) {
              if (objData[line][0] !== "") {  // Each part must have an ID
                let id = objData[line][0];
                let names = extractObjData(line, objData, languages, "Name");
                let desc = extractObjData(line, objData, languages, "Description");
                let picture = objData[line][7];
                let views = { "Default": objData[line][8],
                              "CoupeAA": objData[line][9] };
                let statesPart = states[id];
                let part = new Part(id, names, desc, picture, views, "unloaded",
                                    statesPart, actions[id], false);

                setOfParts[id] = part;
                newItemInListHTML(part, objData[line][10], languages);
              }
            }
            resolve(setOfParts);
          } else {
            reject('Error while parsing config file (see config/obj.csv).');
          }
        }).catch( error => { alert('obj.csv -> ' + error) } );
      }).catch( error => { alert('action.csv -> ' + error) } );
    }).catch( error => { alert('state.csv -> ' + error) } );
  });
}


export let loadPart = (file, name) => {
  return new Promise((resolve, reject) => {
    // file = with extension (a.xxx) | fileName = without extension (a)
    let fileName = file.substr(0, file.lastIndexOf('.')) || file;

    let mtlLoader = new THREE.MTLLoader();
    mtlLoader.setPath('static/parts/Obj/');
    mtlLoader.load(fileName + ".mtl", materials => {
      materials.preload();

      let objLoader = new THREE.OBJLoader();
      objLoader.setPath('static/parts/Obj/');
      objLoader.setMaterials(materials);
      objLoader.load(
        file,
        objLoaded => {
          objLoaded.children[0].material.opacity = 0.2;
          objLoaded.scale.set(SCALE_SCENE, SCALE_SCENE, SCALE_SCENE);
          objLoaded.position.y += Y_PARTS;
          objLoaded.name = name;
          resolve(objLoaded);
        },
        undefined,
        error => { reject(error); }
      );
    });
  });
}


let newItemInListHTML = (part, category, languages) => {
  let id = part.id;
  let list;
  let newItem = "";

  category === "Parts" ?
    list = document.getElementById('listParts') :
    list = document.getElementById('listTools');

  for (let language of languages.areAvailable) {
    newItem +=  "<p class=\"" + language.id + "\">" +
                part.name[language.id] +
                "<span class=\"desc\" id=\"" + language.id + "_" + id + "\">" +
                part.description[language.id] +
                "</span>" +
                "</p>";
  }
  newItem +=  "<img class=\"picture\" src=\"static/parts/Png/" + part.picture + "\">" +
              "<div class=\"settings\">" +
              "<button class=\"loadBtn\" id=\"load " + id + "\">+</button>" +
              "<button class=\"unloadBtn\" id=\"unload " + id + "\">-</button>" +
              "<button class=\"isolatedBtn\">" +
              "<img class=\"isolated\" id=\"isolated " + id + "\" src=\"static/icons/isolated.png\">" +
              "</button>" +
              "<button class=\"transparentBtn\" id=\"transparent " + id + "\">" +
              "<img class=\"transparent\" id=\"isOff " + id + "\" src=\"static/icons/eye.png\">" +
              "<img class=\"transparent\" id=\"isOn " + id + "\" src=\"static/icons/eye-off.png\">" +
              "</button>" +
              "</div>";
  list.innerHTML += newItem;
}


let extractObjData = (line, data, languages, category) => {
  let obj = {};
  for (let indexLine = 1; indexLine < data[0].length; indexLine++) {
    if (data[0][indexLine] === category) {
      for (let i = 0; i < languages.areAvailable.length; i++) {
        if (languages.areAvailable[i].name === data[1][indexLine]) {
          let content = data[line][indexLine];
          if (content === "")
            content = "???";
          obj[languages.areAvailable[i].id] = content;
        }
      }
    }
  }
  return obj;
}


let extractStateData = data => {
  let states = {};
  for (let line = 2; line < data.length; line += 2) {
    let state = {};
    let id = data[line][0];
    if (id.length > 0) {
      if (id !== "") {
        // ---- STATE ASSEMBLED
        let angleA;
        data[line][10] === 'PI' ?
          ( angleA = Math.PI ) : ( angleA = parsed(data[line][7]) || 0 );
        let axisA = new THREE.Vector3(parsed(data[line][4]) || 0,
                                      parsed(data[line][5]) || 0,
                                      parsed(data[line][6]) || 0).normalize();
        let posA = new THREE.Vector3(parsed(data[line][1]) || 0,
                                     parsed(data[line][2]) || 0,
                                     parsed(data[line][3]) || 0);
        let rotA = {
          angle: angleA,
          axis: axisA,
          quaternion: new THREE.Quaternion().setFromAxisAngle(axisA, angleA),
          point: new THREE.Vector3(parsed(data[line][8]) || 0,
                                   parsed(data[line][9]) || 0,
                                   parsed(data[line][10]) || 0)
        };
        state.assembled = {pos: posA, rot: rotA};

        // ---- STATE DISASSEMBLED
        let angleD;
        data[line + 1][10] === 'PI' ?
          ( angleD = Math.PI ) : ( angleD = parsed(data[line + 1][7]) || 0 );
        let axisD = new THREE.Vector3(parsed(data[line + 1][4]) || 0,
                                      parsed(data[line + 1][5]) || 0,
                                      parsed(data[line + 1][6]) || 0).normalize();
        let posD = new THREE.Vector3(parsed(data[line + 1][1]) || 0,
                                     parsed(data[line + 1][2]) || 0,
                                     parsed(data[line + 1][3]) || 0);
        let rotD = {
          angle: angleD,
          axis: axisD,
          quaternion: new THREE.Quaternion().setFromAxisAngle(axisD, angleD),
          point: new THREE.Vector3(parsed(data[line + 1][8]) || 0,
                                   parsed(data[line + 1][9]) || 0,
                                   parsed(data[line + 1][10]) || 0)
        };
        state.disassembled = {pos: posD, rot: rotD};
        states[id] = state;
      }
    }
  }
  return states;
}


let extractActionData = data => {
  let actions = {};
  let lineLength = data[0].length;
  for (let line = 2; line < data.length; line += 2) {
    let action = {};
    let id = data[line][0];
    if (id !== "") {
      action.assemble = {
        toBeAssembled: data[line][lineLength-2].split(','),
        toBeDisassembled: data[line][lineLength-1].split(',')
      };
      action.disassemble = {
        toBeAssembled: data[line+1][lineLength-2].split(','),
        toBeDisassembled: data[line+1][lineLength-1].split(',')
      };
      actions[id] = action;
    }
  }
  return actions;
}


let parsed = data => { return parseFloat(data.replace(',', '.')); }
