// three.js
global.THREE = require('three');
require('three/examples/js/controls/OrbitControls');
require('three/examples/js/loaders/GLTFLoader');

const partsManager = require('./partsManager');
const languages = require('./languages');
const isolated = require('./isolatedView');

export default initScene;


// ------------ Global variables ------------


global.SCALE_SCENE = 0.000125;
global.Y_PARTS = 0;
global.CLOCK = new THREE.Clock();
global.VIEW_TYPE = "Default";
global.SECTIONAL_VIEW_ON = false;

const TIME_ANIMATION = 0.5 // in seconds

let renderer, scene, camera, controls, ratioScene, raycaster, mouse;
let setOfParts, languagesSettings;
let isolatedViewOn = false;
let selectedPartInScene;
let elapsedTime = 0;
let movementVector, targetQuat, animQuat, rotAxis, rotAngle, vecToPoint;
let movingPart;


// ------------ Exported functions ------------


export const initScene = canvasOut => {
  CLOCK.start();
  ratioScene = WIDTH / HEIGHT;

  raycaster = new THREE.Raycaster();
  mouse = new THREE.Vector2();

  renderer = new THREE.WebGLRenderer({ canvas: canvasOut, antialias: true });
  renderer.setSize(WIDTH, HEIGHT);

  scene = new THREE.Scene();
  scene.background = new THREE.Color(0xa0a0a0);

  addLights();

  camera = new THREE.PerspectiveCamera(45, ratioScene, 0.001, 1000);
  camera.position.set(0, 0.1, 0.2);
  camera.lookAt(0, 0, 0);
  scene.add(camera);

  controls = new THREE.OrbitControls(camera, renderer.domElement);
  controls.minDistance = 0.005;
  controls.maxDistance = 0.34;

  // Group that will contain all the parts loaded in the scene
  let group = new THREE.Group();
  group.name = 'partsInScene';
  scene.add(group);

  return new Promise( resolve => {
    languages.loadLanguages().then( l => {
      languagesSettings = l;
      loadPartsInformations().then( () => {
        let gltfloader = new THREE.GLTFLoader();
        gltfloader.load("static/scene/salleTP.gltf", gltf => {
          let content = gltf.scene || gltf.scenes[0];
          content.name = 'workshop';
          content.position.y = -0.1;
          scene.add(content);
          createSkybox();
          resolve(languagesSettings.byDefault);
        });
      });
    });
  });
}


export let openIsolatedView = (id, width, height) => {
  return new Promise( (resolve, reject) => {
    if (id === -1) id = selectedPartInScene.parent.name;
    partsManager.loadPart(setOfParts[id].views.Default).then( obj => {
      obj.name = "obj3D";
      obj.position.setX(0);
      obj.position.setY(0);
      obj.position.setZ(0);
      isolated.openIsolated(obj, width, height);
      isolatedViewOn = true;
      resolve();
    }).catch( err => { reject(err) } );
  });
}


export let closeIsolatedView = () => {
  isolated.closeIsolated();
  isolatedViewOn = false;
}


export let switchTransparentPartInScene = id => {
  let targetParent;
  id !== -1 ?
    targetParent = scene.getObjectByName(id) :
    targetParent = selectedPartInScene.parent;
  for (let target of targetParent.children) {
    target.children[0].material.transparent === true ?
      target.children[0].material.transparent = false :
      target.children[0].material.transparent = true;
  }
  if (selectedPartInScene === undefined)
    return -1;
  return selectedPartInScene.parent.name;
}


export let switchToDefaultView = () => {
  let groupsInScene = scene.getObjectByName('partsInScene').children;
  for (let index = 0; index < groupsInScene.length; ++index) {
    let group = groupsInScene[index];
    group.getObjectByName(VIEW_TYPE).children[0].visible = false;
    group.getObjectByName('Default').children[0].visible = true;
  }
  VIEW_TYPE = 'Default';
  SECTIONAL_VIEW_ON = false;
}


export let switchToSectionalView = () => {
  let groupsInScene = scene.getObjectByName('partsInScene').children;
  for (let index = 0; index < groupsInScene.length; ++index) {
    let group = groupsInScene[index];
    group.getObjectByName(VIEW_TYPE).children[0].visible = false;
    group.getObjectByName('Sectional').children[0].visible = true;
  }
  VIEW_TYPE = 'Sectional';
  SECTIONAL_VIEW_ON = true;
}


export let selectLanguage = languageId => {
  for (let index = 0; index < languagesSettings.areAvailable.length; index++) {
    languagesSettings.areAvailable[index].isSelected = false;
    if (languagesSettings.areAvailable[index].id === languageId)
      languagesSettings.areAvailable[index].isSelected = true;
  }
}


export let loadPartInScene = id => {
  return new Promise( (resolve, reject) => {
    if (setOfParts[id].currentState !== "loaded") {
      let objDefault = partsManager.loadPart(setOfParts[id].views.Default, 'Default');
      let objSectional = partsManager.loadPart(setOfParts[id].views.CoupeAA, 'Sectional');

      Promise.all([objDefault, objSectional]).then( promises => {
        let objGroup = new THREE.Group();

        objGroup.name = id;
        for (let index = 0; index < promises.length; ++index) {
          if (promises[index].name !== VIEW_TYPE)
            promises[index].children[0].visible = false;
          objGroup.add(promises[index]);
        }

        scene.getObjectByName('partsInScene').add(objGroup);

        let min = 0;
        for (let index = 0; index < objGroup.children.length; index++) {
          let boundaries = new THREE.Box3().setFromObject(objGroup.children[index]);
          if (boundaries.min.y < min) min = boundaries.min.y;
        }

        if (min !== 0) {
          Y_PARTS -= min;
          for (let partGroup of scene.getObjectByName('partsInScene').children)
            for (let part of partGroup.children)
              part.position.y -= min;
        }

        let partDisassembled = setOfParts[id].states.disassembled;
        objGroup.position.setX(partDisassembled.pos.x * SCALE_SCENE);
        objGroup.position.setY(partDisassembled.pos.y * SCALE_SCENE);
        objGroup.position.setZ(partDisassembled.pos.z * SCALE_SCENE);

        objGroup.quaternion.setFromAxisAngle(partDisassembled.rot.axis,
                                              partDisassembled.rot.angle);
        setOfParts[id].currentState = "loaded";
      }).catch( err => { reject(err) } );
    } else reject('Part already loaded in scene !');
    resolve(true);
  });
}


export let unloadPartInScene = id => {
  if (id === -1)
    id = selectedPartInScene.parent.name;
  if (!setOfParts[id].isAssembled) {
    let subscene = scene.getObjectByName('partsInScene');
    subscene.remove(subscene.getObjectByName(id));
    setOfParts[id].currentState = "unloaded";
    setOfParts[id].isAssembled = false;
    return [true, id];
  } else {
    alert('The piece must be disassembled to be removed !');
    return [false];
  }
}


export let detectPart = (mouseX, mouseY) => {
  mouse.x = mouseX;
  mouse.y = mouseY;
  if (!movingPart) {
    let intersects = [];
    raycaster.setFromCamera(mouse, camera);

    for (let objGroup of scene.getObjectByName('partsInScene').children) {
      for (let obj of objGroup.children) {
        let tmp = raycaster.intersectObjects(obj.children);
        if (tmp.length > 0)
          intersects.push(obj);
      }
    }

    if (intersects.length > 0) {
      let i = intersects.length - 1;
      selectedPartInScene = intersects[i];
      return {isClicked: true,
              isAssembled: setOfParts[intersects[i].parent.name].isAssembled,
              isTransparent: intersects[i].children[0].material.transparent};
    }
  }
  return {isClicked: false};
}


export let actionToDo = action => {
  if (SECTIONAL_VIEW_ON) { return false; }

  switch (action) {
    case 'assemble':
      if (!setOfParts[selectedPartInScene.parent.name].isAssembled) {
        if (canAssemblePart(selectedPartInScene.parent.name)) {
          manipulatePart(selectedPartInScene.parent.name);
          setOfParts[selectedPartInScene.parent.name].isAssembled = true;
          return true;
        }
      }
      break;
    case 'disassemble':
      if (setOfParts[selectedPartInScene.parent.name].isAssembled) {
        if (canDisassemblePart(selectedPartInScene.parent.name)) {
          manipulatePart(selectedPartInScene.parent.name);
          setOfParts[selectedPartInScene.parent.name].isAssembled = false;
          return true;
        }
      }
      break;
    default:
      break;
  }
  return false;
}


export const updateScene = (width, height) => {
  ratioScene = width / height;
  renderer.setSize(width, height);
  camera.aspect = ratioScene;
  camera.updateProjectionMatrix();

  controls.update();
  if (camera.position.y < -0.085) controls.reset();
  controls.saveState();

  let table_montage = scene.getObjectByName("Table_montage");
  if (table_montage !== undefined) {
    controls.getPolarAngle() > Math.PI/2 ?
      ( table_montage.visible = false ) : ( table_montage.visible = true )
  }

  if (movingPart) {
    let delta = CLOCK.getDelta();
    if (delta + elapsedTime < TIME_ANIMATION) {
      elapsedTime += delta;

      selectedPartInScene.parent.position.x += movementVector.x * (delta / TIME_ANIMATION);
      selectedPartInScene.parent.position.y += movementVector.y * (delta / TIME_ANIMATION);
      selectedPartInScene.parent.position.z += movementVector.z * (delta / TIME_ANIMATION);

      translate(selectedPartInScene.parent, vecToPoint);
      let angleTmp = rotAngle * delta / TIME_ANIMATION;
      animQuat = new THREE.Quaternion().setFromAxisAngle(rotAxis, angleTmp);
      selectedPartInScene.parent.applyQuaternion(animQuat);
      translate(selectedPartInScene.parent, vecToPoint.clone().negate());
    } else {
      selectedPartInScene.parent.position.x += movementVector.x * ((TIME_ANIMATION - elapsedTime) / TIME_ANIMATION);
      selectedPartInScene.parent.position.y += movementVector.y * ((TIME_ANIMATION - elapsedTime) / TIME_ANIMATION);
      selectedPartInScene.parent.position.z += movementVector.z * ((TIME_ANIMATION - elapsedTime) / TIME_ANIMATION);

      translate(selectedPartInScene.parent, vecToPoint);
      let angleTmp =  selectedPartInScene.parent.quaternion.angleTo(targetQuat);
      animQuat = new THREE.Quaternion().setFromAxisAngle(rotAxis, angleTmp);
      selectedPartInScene.parent.applyQuaternion(animQuat);
      translate(selectedPartInScene.parent, vecToPoint.clone().negate());

      movingPart = false;
      elapsedTime = 0;
    }
  }
  isolated.update(width, height);
}


export let renderScene = () => {
  if (!isolatedViewOn) renderer.render(scene, camera);
  else isolated.renderIsolated();
}


// ------------ Internal functions ------------


let loadPartsInformations = () => {
  return new Promise( resolve => {
    partsManager.loadPartsInformations(languagesSettings).then( informations => {
      setOfParts = informations;
      resolve();
    }).catch( error => { alert(error); });
  });
}


const addLights = () => {
  let hemiLight = new THREE.HemisphereLight(0xFDB8C8);
  hemiLight.groundColor.set(0xeeeeee);
  hemiLight.position.set(0, 10, 0);
  scene.add(hemiLight);

  let pointLight = new THREE.PointLight(0xffffff);
  pointLight.position.set(0, 0.5, 0);
  scene.add(pointLight);
}


const createSkybox = () => {
  // Cubemap order -> +x / -x / +y / -y / +z / -z
  let files = ["negz.jpg", "posz.jpg", "posy.jpg", "negy.jpg", "posx.jpg",
              "negx.jpg"];
  let materials = [];

  for (let i = 0; i < files.length; i++) {
    let texture = new THREE.TextureLoader().load('static/skybox/' + files[i]);
    materials.push(new THREE.MeshBasicMaterial( { map: texture } ));
    materials[i].side = THREE.BackSide;
  }

  let geometry = new THREE.BoxBufferGeometry(1.5, 1.5, 1.5);
  scene.add(new THREE.Mesh(geometry, materials));
}


let canAssemblePart = id => {
  let parents = setOfParts[id].actions.assemble;
  if (parents.toBeAssembled[0] !== "") {
    for (let idElem of parents.toBeAssembled) {
      if (!setOfParts[idElem].isAssembled) {
        alert ('Can\'t assemble this piece. You should do something with ' +
                setOfParts[idElem].name[languagesSettings.byDefault]);
        return false;
      }
    }
  }
  if (parents.toBeDisassembled[0] !== "") {
    for (let idElem of parents.toBeDisassembled) {
      if (setOfParts[idElem].isAssembled) {
        alert ('Can\'t assemble this piece. You should do something with ' +
                setOfParts[idElem].name[languagesSettings.byDefault]);
        return false;
      }
    }
  }
  if (movingPart)
    return false;

  return true;
}


let canDisassemblePart = id => {
  let parents = setOfParts[id].actions.disassemble;
  if (parents.toBeAssembled[0] !== "") {
    for (let idElem of parents.toBeAssembled) {
      if (!setOfParts[idElem].isAssembled) {
        alert ('Can\'t assemble this piece. You should do something with ' +
                setOfParts[idElem].name[languagesSettings.byDefault]);
        return false;
      }
    }
  }
  if (parents.toBeDisassembled[0] !== "") {
    for (let idElem of parents.toBeDisassembled) {
      if (setOfParts[idElem].isAssembled) {
        alert ('Can\'t assemble this piece. You should do something with ' +
                setOfParts[idElem].name[languagesSettings.byDefault]);
        return false;
      }
    }
  }
  if (movingPart)
    return false;

  return true;
}


let manipulatePart = id => {
  let state, currentState;
  let movementStartPos = new THREE.Vector3;

  setOfParts[id].isAssembled ?
    ( state = setOfParts[id].states.disassembled,
      currentState = setOfParts[id].states.assembled ) :
    ( state = setOfParts[id].states.assembled,
      currentState = setOfParts[id].states.disassembled );

  rotAxis = state.rot.axis;
  rotAngle = state.rot.angle - currentState.rot.angle;
  targetQuat = state.rot.quaternion;
  selectedPartInScene.parent.getWorldPosition(movementStartPos);
  movementVector = state.pos.clone()
                            .sub(currentState.pos)
                            .multiplyScalar(SCALE_SCENE);
  vecToPoint = state.rot.point.clone()
                              .multiplyScalar(SCALE_SCENE)
                              .sub(movementStartPos);
  movingPart = true;
  CLOCK.start();
}


let translate = (part, point) => {
  part.translateX(point.x);
  part.translateY(point.y);
  part.translateZ(point.z);
}
