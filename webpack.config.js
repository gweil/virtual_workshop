const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const dev = process.env.NODE_ENV == "dev";

let config = {
  entry: __dirname + '/js/app.js',
  output: {
    path: __dirname + '/dist',
    filename: 'app.bundle.js',
    library: 'virtual_workshop',
    libraryTarget: 'umd',
    publicPath: "/dist/"
  },
  watch: dev,
  mode: dev ? 'development' : 'production',
  devtool: dev ? "cheap-module-eval-source-map" : "source-map",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  plugins: []
}

if (!dev) {
  config.plugins.push(new UglifyJsPlugin( { sourceMap: true } ));
}

module.exports = config
